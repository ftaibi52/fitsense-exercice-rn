export const fetchPastChallenges = async () => {
  const URL = 'https://demo.api.activelife.io/coding_exercise';
  try {
    let response = await fetch(URL);
    let data = await response.json();
    return data;
  } catch (error) {
    console.error(error);
  }
};
