/* libs */
import React from 'react';
import {
  View,
  Image,
  StyleSheet,
  Platform,
} from 'react-native';
import Header from './Header';
import ProgressBar from './ProgressBar';

import {Icon} from '../../../components/Arrow';
import Text from '../../../components/Text';

const Challenge = ({data}) => (
  <View style={styles.card}>
    <View style={styles.viewImg}>
      <Image
        style={styles.img}
        source={{
          uri: data.image_url,
        }}
      />
    </View>
    <View style={styles.column}>
      <View style={styles.row}>
        <Header style={styles.name} title={data.name} />
        <Text style={styles.date} >
          {data.date}
        </Text>
      </View>
      <View style={styles.row}>
        <View>
          {data.type === 'timer' ? (
            <View style={styles.row}>
              <Text style={styles.score}>{data.activity_score}</Text>
              <Text style={styles.score_goal}>
                / {data.activity_score_goal}
              </Text>
            </View>
          ) : (
            <View style={styles.row}>
              <Text style={styles.score}>#{data.rank}</Text>
              <Text style={styles.score_goal}> / {data.participants}</Text>
            </View>
          )}
        </View>
        <View>
          {data.type === 'timer' ? (
            data.won ? (
              <Icon
                name="tick"
                color="white"
                extraStyles={data.won ? styles.icon : styles.icon2}
                whenPushed={() => {
                  console.log('icon pushed');
                }}
              />
            ) : (
              <Icon
                name="cross"
                color="white"
                extraStyles={data.won ? styles.icon : styles.icon2}
                whenPushed={() => {
                  console.log('icon pushed');
                }}
              />
            )
          ) : data.won ? (
            <Icon
              name="star"
              color="white"
              extraStyles={data.won ? styles.icon : styles.icon2}
              whenPushed={() => {
                console.log('icon pushed');
              }}
            />
          ) : (
            <Icon
              name="cross"
              color="white"
              extraStyles={data.won ? styles.icon : styles.icon2}
              whenPushed={() => {
                console.log('icon pushed');
              }}
            />
          )}
        </View>
      </View>
      <View>
        {data.type === 'leaderboard' ? null : (
          <ProgressBar
            progress={data.activity_score}
            full_progress={data.activity_score_goal}></ProgressBar>
        )}
      </View>
    </View>
  </View>
);

const styles = StyleSheet.create({
  card: {
    flexDirection: 'row',
    borderRadius: 4,
    borderColor: '#dedede',
    borderWidth: 0.5,
    marginTop: 15,
    marginBottom: 5,
    backgroundColor: 'white',
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowRadius: 2,
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.16,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  viewImg: {
    flexDirection: 'row',
    width: '30%',
  },
  img: {
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    width: '100%',
  },
  name: {
    width: "75%",
  },
  date:{
    width:"25%",
    marginLeft: 5,
    textAlign: "right"
    
  },  
  column: {
    width: '70%',
    paddingTop: 12.5,
    paddingBottom: 17.5,
    paddingRight: 12.5,
    paddingLeft: 17,
  },

  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  icon: {
    alignSelf: 'flex-end',
    backgroundColor: 'rgb(27, 173, 181)',
    padding: 2,
    borderRadius: 10,
  },

  icon2: {
    backgroundColor: '#a1a1a1',
    padding: 2,
    borderRadius: 10,
  },

  score_goal: {
    color: 'rgb(161, 161, 161)',
  },
});

export default Challenge;
