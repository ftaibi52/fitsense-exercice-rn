/* libs */
import React from "react";
import { View } from "react-native";

const ProgressBar = ({ progress, full_progress }) => (
    
  <View
    style={{
      width: full_progress + "%",
      marginTop: 10,
      flexDirection: "row",
      height: 4.5,
      backgroundColor: "rgb(247, 247, 247)",
      borderColor: "#000",
      borderRadius: 15,
    }}
  >
    <View
      style={{
        width: progress >= 100 ? "100%" : progress + "%",
        borderRadius: 15,
        backgroundColor: "rgb(4, 175, 180)"
      }}
    ></View>
  </View>
);

export default ProgressBar;
