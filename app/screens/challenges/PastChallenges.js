import React, {Component} from 'react';
import {ActivityIndicator, View, SafeAreaView} from 'react-native';
import {connect} from 'react-redux';

/* Components */
import Timeline from '../../components/Timeline';
import Arrow from '../../components/Arrow';
import NavTop from '../../components/NavTop';
import Text from '../../components/Text';
import Page from '../../components/Page';
import Challenge from './components/Challenge';

/* Styles */
import {DARK} from '@styles/colors';
import appStyles from '@styles/global';

import {STRINGS} from '@localization/Strings';
import {fetchPastChallenges} from '../../utils/api';

const mapStateToProps = state => ({
  past: state.challenges.past,
  inProgress: state.challenges.in_progress,
});

const mapDispatchToProps = dispatch => ({
  getChallenges: payload => dispatch({type: 'GET_PASTCHALLENGES', payload}),
  makeRequest: () => dispatch({type: 'MAKE_CHALLENGE_REQUEST'}),
  resetRequest: () => dispatch({type: 'RESET_CHALLENGE_REQUEST'}),
});

class PastChallenges extends Component {
  constructor(props) {
    super(props);

    this.state = {
      response: [],
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.past !== this.props.past) {
      this.setChallengesState();
    }
  }

  setChallengesState = () => {
    const {past} = this.props;

    this.setState({
      response: past,
    });
  };

  async componentDidMount() {
    const {makeRequest, resetRequest, getChallenges} = this.props;
    makeRequest();
    try {
      const pastChallengesResponse = await fetchPastChallenges();
      getChallenges(pastChallengesResponse);
    } catch (error) {
      console.error(error);
    } finally {
      resetRequest();
    }
  }

  render() {
    const {goBack} = this.props.navigation;
    const {inProgress} = this.props;

    if (inProgress) {
      return (
        <Page title={STRINGS.PastChallenges}>
          <ActivityIndicator size="small" color={DARK} />
        </Page>
      );
    }
    return (
      <Page>
        <NavTop
          showUnderline={true}
          noStatusBarHeight={true}
          negativePadding={true}
          noPadding={false}>
          <Arrow
            direction={'left'}
            color={'black'}
            whenPushed={() => goBack()}
          />
        </NavTop>
        <View style={appStyles.paddedContainer}>
          <Text
            style={{paddingTop: 15, paddingBottom: 15, paddingLeft: 21}}
            bold>
            {STRINGS.PastChallenges}
          </Text>
          <View>
            {this.state.response.map(x => {
              return (
                <Timeline
                  key={x.month_year.toString()}
                  title={x.month_year}
                  children={x.challenges.map(element => {
                    return (
                      <Challenge
                        key={element.name.toString()}
                        data={element}></Challenge>
                    );
                  })}
                />
              );
            })}
          </View>
        </View>
      </Page>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PastChallenges);
